export class CreateOrderDto {
  orderItems: {
    prodictId: number;
    qty: number;
  }[];
  userId: number;
}
